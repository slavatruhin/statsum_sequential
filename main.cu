#include <iostream>
#include <fstream>
#include <vector>
#include <chrono>

#define n 10
#define J 1

struct Chain
{
    int8_t s[n]{};
};

struct Gem
{
    int32_t G=0;
    int16_t E=0;
    int16_t M=0;
    std::vector<uint32_t>conf;
};

struct Gem_part
{
    int16_t E=0;
    int16_t M=0;
    uint32_t conf=0;
    bool l = false;
};

void thread_init(std::vector<Chain>&chain, int length)
{
    for (auto i=0; i<length; i++)
    {
        int bit = i;
        for (auto & j : chain[i].s)
        {
            j = bit & 1 ? 1 : -1;
            bit>>=1;
        }
    }
}

void generator_gem_o(std::vector<Chain>&chain, int length, std::vector<Gem_part>&gem_part)
{
    for(int i=0; i<length; i++)
    {
        for(int j=0; j<n; j++)
        {
            gem_part[i].E += -J * chain[i].s[j] * chain[i].s[(j + 1) % n];
            gem_part[i].M += chain[i].s[j];
        }
        gem_part[i].conf = (uint32_t)i;
        gem_part[i].l = true;
    }
}

void sort(std::vector<Gem_part>&gem_in, std::vector<Gem>&gem_out)
{
    for(int i=0; i<gem_in.size(); i++)
    {
        if(!gem_in[i].l) break;
        bool out = true;
        for(int j=0; j<gem_out.size(); j++)
        {
            out = false;
            if(gem_out[j].E == gem_in[i].E && gem_out[j].M == gem_in[i].M)
            {
                gem_out[j].G++;
                for(int k=0; k<gem_out[j].conf.size(); k++)
                {
                    if(gem_out[j].conf[k] == gem_in[i].conf) break;
                    if(k==gem_out[j].conf.size()-1)
                    {
                        gem_out[j].conf.resize(gem_out[j].conf.size()+1);
                        gem_out[j].conf[gem_out[j].conf.size()-1]=gem_in[i].conf;
                    }
                }
                break;
            }
            if(j==(gem_out.size()-1))
            {
                out = true;
            }
        }
        if(out)
        {
            gem_out.resize(gem_out.size() + 1);
            gem_out[gem_out.size()-1].G = 1;
            gem_out[gem_out.size()-1].E = gem_in[i].E;
            gem_out[gem_out.size()-1].M = gem_in[i].M;
            gem_out[gem_out.size()-1].conf.resize(1);
            gem_out[gem_out.size()-1].conf[0] = gem_in[i].conf;
        }
    }
}

void outputs(std::vector<Gem>&gem_o, std::vector<Gem>&gem_output)
{
    std::ofstream file_o("Gem_o.txt");
    std::ofstream file_output("Gem_output.txt");
    int G_sum = 0;
    if(n<4)
    {
        std::cout << "gem original:\n";
        for (auto &i : gem_o)
        {
            std::cout << "G=" << i.G
                      << " E=" << i.E
                      << " M=" << i.M
                      << " configurations:";
            for (unsigned int j : i.conf)
            {
                std::cout << " " << j;
            }
            std::cout << "\n";
        }
        std::cout << "\ngem odd:\n";
        for (auto &i : gem_output)
        {
            std::cout << "G=" << i.G
                      << " E=" << i.E
                      << " M=" << i.M
                      << " configurations:";
            for (unsigned int j : i.conf)
            {
                std::cout << " " << j;
            }
            std::cout << "\n";
        }
        for (auto &i : gem_o)
        {
            file_o << "G=" << i.G
                   << " E=" << i.E
                   << " M=" << i.M
                   << " configurations:";
            for (unsigned int j : i.conf)
            {
                file_o << " " << j;
            }
            file_o << "\n \n";
        }
        for (auto &i : gem_output)
        {
            G_sum += i.G;
            file_output << "G=" << i.G
                 << " E=" << i.E
                 << " M=" << i.M
                 << " configurations:";
            for (unsigned int j : i.conf)
            {
                file_output << " " << j;
            }
            file_output << "\n \n";
        }
        std::cout << "Sum of G = " << G_sum << "\n";
    }
    else
    {
        for (auto &i : gem_o)
        {
            file_o << "G=" << i.G
                 << " E=" << i.E
                 << " M=" << i.M
                 << " configurations:";
            for (unsigned int j : i.conf)
            {
                file_o << " " << j;
            }
            file_o << "\n \n";
        }
        for (auto &i : gem_output)
        {
            G_sum += i.G;
            file_output << "G=" << i.G
                     << " E=" << i.E
                     << " M=" << i.M
                     << " configurations:";
            for (unsigned int j : i.conf)
            {
                file_output << " " << j;
            }
            file_output << "\n \n";
        }
        file_output << "Sum of G = " << G_sum << "\n";
    }
}

void odd(std::vector<Gem>&gem_o, std::vector<Gem>&gem_even, std::vector<Gem>&gem_odd,
         std::vector<Gem_part>&gem_part, std::vector<Chain>&chain)
{
    for(int i=0; i<gem_even.size(); i++)
    {
        for(int j=0; j<gem_o.size(); j++)
        {
            for(int k=0; k<gem_even[i].conf.size(); k++)
            {
                gem_part.resize(gem_o[j].conf.size());
                for(int m=0; m<gem_part.size(); m++)
                {
                    gem_part[m].E=0;
                    gem_part[m].M=0;
                    gem_part[m].conf=0;
                    gem_part[m].l=false;
                }
                for(int l=0; l<gem_o[j].conf.size();l++)
                {
                    gem_part[l].E = gem_even[i].E + gem_o[j].E;
                    for(int m=0; m<n; m++)
                    {
                        gem_part[l].E += J * chain[gem_even[i].conf[k]].s[m] * chain[gem_o[j].conf[l]].s[m];
                    }
                    gem_part[l].M = gem_even[i].M + gem_o[j].M;
                    gem_part[l].conf = gem_o[j].conf[l];
                    gem_part[l].l = true;
                }
                sort(gem_part, gem_odd);
            }
        }
    }
}

void even(std::vector<Gem>&gem_o, std::vector<Gem>&gem_even, std::vector<Gem>&gem_odd,
          std::vector<Gem_part>&gem_part, std::vector<Chain>&chain)
{
    for(int i=0; i<gem_odd.size(); i++)
    {
        for(int j=0; j<gem_o.size(); j++)
        {
            for(int k=0; k<gem_odd[i].conf.size(); k++)
            {
                gem_part.resize(gem_o[j].conf.size());
                for(int m=0; m<gem_part.size(); m++)
                {
                    gem_part[m].E=0;
                    gem_part[m].M=0;
                    gem_part[m].conf=0;
                    gem_part[m].l=false;
                }
                for(int l=0; l<gem_o[j].conf.size();l++)
                {
                    gem_part[l].E = gem_odd[i].E + gem_o[j].E;
                    for(int m=0; m<n; m++)
                    {
                        gem_part[l].E += J * chain[gem_odd[i].conf[k]].s[m] * chain[gem_o[j].conf[l]].s[m];
                    }
                    gem_part[l].M = gem_odd[i].M + gem_o[j].M;
                    gem_part[l].conf = gem_o[j].conf[l];
                    gem_part[l].l = true;
                }
                sort(gem_part, gem_even);
            }
        }
    }
}

int main()
{
    auto t1 = std::chrono::high_resolution_clock::now();
    int length = 1<<n;
    std::vector<Chain>chain(length);
    std::vector<Gem_part>gem_part(length);
    std::vector<Gem>gem_o;
    std::vector<Gem>gem_even;
    std::vector<Gem>gem_odd;
    thread_init(chain, length);
    generator_gem_o(chain, length, gem_part);
    sort(gem_part, gem_o);
    gem_even = gem_o;
    for(int i=0; i<n; i++)
    {
        if(!(i%2))
        {
            odd(gem_o, gem_even, gem_odd, gem_part, chain);
        }
        if(i%2)
        {
            even(gem_o, gem_even, gem_odd, gem_part, chain);
        }
    }
    if(!(n%2))
    {
        outputs(gem_o, gem_odd);
    }
    if(n%2)
    {
        outputs(gem_o, gem_even);
    }
    auto t2 = std::chrono::high_resolution_clock::now();
    auto time = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count();
    std::cout << "working time is " << time/3600000 << " h " << (time%3600000)/60000 << " m "
              << ((time%3600000)%60000)/1000 << " s " << ((time%3600000)%60000)%1000 << " ms \n";
}